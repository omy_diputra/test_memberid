import express from "express";
import {
    getProducts,
    getProductBySearch,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
} from "../controllers/Products.js"; // manggil method/fungsi
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

//buat method dengan endpoint
router.get('/products', verifyUser, getProducts);
router.get('/products/:name', verifyUser, getProductBySearch);
router.get('/products/:id', verifyUser, getProductById);
router.post('/products', verifyUser, createProduct);
router.patch('/products/:id', verifyUser, updateProduct);
router.delete('/products/:id', verifyUser, deleteProduct);

export default router;