import express from "express";
import {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
} from "../controllers/Users.js"; // manggil method/fungsi
import { verifyUser, adminOnly } from "../middleware/AuthUser.js";

const router = express.Router();

//buat method dengan endpoint
//router.get('/users, getUserById); ini contoh tidak secure
router.get('/users', verifyUser, adminOnly , getUsers);
router.get('/users/:id', verifyUser , adminOnly , getUserById);
router.post('/users', createUser);
router.patch('/users/:id', verifyUser , adminOnly , updateUser);
router.delete('/users/:id', verifyUser , adminOnly , deleteUser);

export default router;