import express from "express";
import cors from "cors";
import session from "express-session";
import dotenv from "dotenv";
// import db from "./config/Database.js"; // untuk mengenerate table secara otomatis
import SequelizStore from "connect-session-sequelize";
import UserRoute from "./routes/UserRoute.js";
import ProductRoute from "./routes/ProductRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
import db from "./config/Database.js";
dotenv.config();

const app = express();

const sessionStore = SequelizStore(session.Store);

const store = new sessionStore({
  db: db
});

// lakukan sycn
// (async()=>{
//     await db.sync();
// })();

//defenisikan session
//dari env
// ada random string "SESS_SECRET" semakin rumit semakin bagus
app.use(session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
      // klo "true" dia pasti menggunakan HTTPS tapi klo "auto" , itu tergantung detect nya apa
      secure: "auto",
    },
  })
);

//disini untuk akses ke API kita
//defult port untuk react 3000
app.use(
  cors({
    credentials: true,
    origin: ["http://localhost:3000"],
  })
);
//agar kita bisa menerima data format json
app.use(express.json());
// gunakan sebagai medleware --------------
app.use(UserRoute);
app.use(ProductRoute);
app.use(AuthRoute);

// store.sync();

app.listen(process.env.APP_PORT, () => {
  console.log("Server up and running . . .");
});
