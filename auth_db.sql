-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2023 at 09:22 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auth_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `poin` int(11) NOT NULL,
  `giftcard` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uuid`, `name`, `poin`, `giftcard`, `userId`, `createdAt`, `updatedAt`) VALUES
(1, '887479e5-0e24-42c6-bdcd-005d4ee54c4d', 'Product 1', 9736, 10000, 1, '2023-02-03 18:59:15', '2023-02-03 18:59:15'),
(2, 'bb6f8c03-d68c-4300-b4bf-918d9fb65a6f', 'Product 2', 2000, 20000, 1, '2023-02-03 18:59:37', '2023-02-03 18:59:37'),
(3, 'c5b0a3db-0db3-4e2d-b523-e34d91143403', 'Product 3', 3000, 30000, 1, '2023-02-03 18:59:52', '2023-02-03 18:59:52');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(36) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`sid`, `expires`, `data`, `createdAt`, `updatedAt`) VALUES
('lJtRy-7PVgfAAiv8OGCv-CqCjdwhFhmM', '2023-02-04 19:44:51', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"b793bc8d-ad2a-4392-b9ce-02cabeb284db\"}', '2023-02-03 18:47:46', '2023-02-03 19:44:51'),
('qEWTEoKfZ_eCi-fAcZCKjcEGSPsk9chz', '2023-02-04 20:15:41', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"b793bc8d-ad2a-4392-b9ce-02cabeb284db\"}', '2023-02-03 19:08:05', '2023-02-03 20:15:41'),
('s22_ecsazvDe35zst07xdueZP24RWV79', '2023-02-04 19:08:05', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2023-02-03 19:08:05', '2023-02-03 19:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 'b793bc8d-ad2a-4392-b9ce-02cabeb284db', 'Admin romy', 'admin@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$ia7Z9Qvztz9oF/dzMX3qag$QfFN9FsLidTul5SLRdudUc9EPOSK8tksd0/LcDaYnt8', 'admin', '2023-02-03 18:51:35', '2023-02-03 18:51:35'),
(2, '103d645f-3493-458c-80a4-0b0927569918', 'User romy', 'user@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$IWeWQqbNS6iAJFwg6nfVkw$PDzb9bJDXfJ4Vaot69kRcEDYvu4KyctguIxDoqBTQWE', 'admin', '2023-02-03 18:51:51', '2023-02-03 18:51:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
