import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./components/Login";
import Products from "./pages/Products";

function App() {
  return (
    <div >
      <BrowserRouter>
        <Routes>
          {/* render routes*/}
          <Route path="/" element={<Login />}></Route>
          <Route path="/products" element={<Products />}></Route>



        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
