import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    user: null,
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ""
}

export const LoginUser = createAsyncThunk("user/LoginUser", async(user, thunkAPI) => {
    try {
        const response = await axios.post('http://localhost:5000/login', {//ENDPOINT
            email: user.email,
            password: user.password
        });
        return response.data;
    } catch (error) {
        if(error.response){
            //ngmabil pesan error dari backend
            // msg adalah variale dari backend
            const message = error.response.data.msg;
            return thunkAPI.rejectWithValue(message);
        }
    }
});
//ini untuk data session dan ini dari yang sudah login , fungsi ini ada di backend, kita tidak butuh data
//makanya di async nya (_
export const getMe = createAsyncThunk("user/getMe", async(_, thunkAPI) => {
    try {
        const response = await axios.get('http://localhost:5000/me');//ENDPOINT
        return response.data;
    } catch (error) {
        if(error.response){
            const message = error.response.data.msg;
            return thunkAPI.rejectWithValue(message);
        }
    }
});

export const LogOut = createAsyncThunk("user/LogOut", async() => {
    await axios.delete('http://localhost:5000/logout');//ENDPOINT
});

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers:{
        reset: (state) => initialState
        //berfungsi untuk mereset
    },
    extraReducers:(builder) =>{
        builder.addCase(LoginUser.pending, (state) =>{
            //disaat dia pending loading nya true
            state.isLoading = true;
        });
        builder.addCase(LoginUser.fulfilled, (state, action) =>{
            //klo login berhasil, loadingnya false
            state.isLoading = false;
            state.isSuccess = true;
            state.user = action.payload;//karna kita punya data didalam payload LoginUser
        });
        builder.addCase(LoginUser.rejected, (state, action) =>{
            // klo login gagal
            state.isLoading = false;
            state.isError = true;
            state.message = action.payload;
        })

        // Get User yang Login atau extraReducers untuk getMe diatas
        builder.addCase(getMe.pending, (state) =>{
            state.isLoading = true;
        });
        builder.addCase(getMe.fulfilled, (state, action) =>{
            state.isLoading = false;
            state.isSuccess = true;
            state.user = action.payload;
        });
        builder.addCase(getMe.rejected, (state, action) =>{
            state.isLoading = false;
            state.isError = true;
            state.message = action.payload;
        })
    }
});

export const {reset} = authSlice.actions;//ini dari authSlice
export default authSlice.reducer;