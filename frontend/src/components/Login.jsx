import React, { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";//untuk redirect setelah login
import { LoginUser, reset } from "../features/authSlice";
import logo from "../logo_test.png";


const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { user, isError, isSuccess, isLoading, message } = useSelector(
        (state) => state.auth
    );

    useEffect(() => {
        if (user || isSuccess) {//cek data, klo suksek direct ke dashboard
            navigate("/products");
        }
        dispatch(reset());//reset
    }, [user, isSuccess, dispatch, navigate]);

    const Auth = (e) => {//fungsi action form
        e.preventDefault();//agar ketika submit pagenya tidak reload
        dispatch(LoginUser({ email, password }));// dispatch ini supaya usernya ter set
    };
    const mystyle = {
        display: "block",
        marginLeft: "auto",
        marginRight: "auto"
    };

    return (
        <section className="hero has-background-grey-light is-fullheight is-fullwidth">
            <div className="hero-body">
                <div className="container">
                    <div className="columns is-centered">
                        <div className="column is-4">
                            <form onSubmit={Auth} className="box">
                                <img style={mystyle} src={logo} width="150" alt="logo" />
                                <h1 className="title is-2 has-text-centered mt-5">AWARD</h1>
                                <h4 className="is-2 has-text-centered">Enter your email address <br /> to sign in and continue</h4>
                                {/*cek error apa tidak atau render*/}
                                {isError &&
                                    <div class="notification is-danger mt-5">
                                        <p className="has-text-centered">{message}</p>
                                    </div>
                                }
                                <div className="field mt-5">
                                    <div className="control">
                                        <input type="text" className="input" value={email} onChange={(e) => setEmail(e.target.value)} placeholder='Email . . . ' />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className='label'>Password</label>
                                    <div className="control">
                                        <input type="password" className="input" value={password} onChange={(e) => setPassword(e.target.value)} placeholder='* * * * * * ' />
                                    </div>
                                </div>
                                <div className="field mt-5">
                                    <button type="submit" className="button is-success is-fullwidth">
                                        {isLoading ? "Loading..." : "Login"}
                                        {/* jika lodingnya true maka akan render "Loading..." */}
                                        {/* jika lodingnya false maka akan render "Login" */}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login