import React, { useState, useEffect } from "react";

import axios from "axios";// ini dipanggil untuk interaksi API


const ProductList = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        getProducts();
    }, []);

    const getProducts = async () => {
        const response = await axios.get("http://localhost:5000/products");
        setProducts(response.data);
    };
    return (
        <div>
            <div className="container">
                <div className="columns is-centered">
                    <div className="column is-4">
                        {products.map((product, index) => (
                            <div><div className="box small">
                                <div className="has-text-right"><h4 className='label tag is-link'>{product.name}</h4></div>
                                <div className="field mt-5">
                                    <div className="control">
                                    </div>
                                </div>
                                <div className="field">
                                    <label>{product.poin} Poin</label>
                                </div>
                            </div><h4 className="is-2 label mb-5">Gift Card IDR {product.giftcard}</h4>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductList